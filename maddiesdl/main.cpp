/*This source code copyrighted by Lazy Foo' Productions (2004-2015)
and may not be redistributed without written permission.*/

//Using SDL and standard IO
#include <SDL.h>
#include <SDL_image.h>
#include <SDL_mixer.h>

#include <stdio.h>
#include <string>
#include <cstdlib>

#include "LTexture.h"

//Screen dimension constants
const int SCREEN_WIDTH = 1152;
const int SCREEN_HEIGHT = 648;

SDL_Window* gWindow = NULL; // The window
SDL_Surface* gScreenSurface = NULL; // The surface of the window



SDL_Renderer* gRenderer = NULL;

LTexture* gBackground;
LTexture* gOtherImage;
SDL_Rect gSpriteClips[3];

class ScrnPs
{
public:
	int x;
	int y;
};


bool init()
{
	bool success = true;


	//Init SDL
	if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO) < 0)
	{
		printf("SDL could not initialize! SDL Error: %s\n", SDL_GetError());
		success = false;
	}
	else
	{
		//Create the window
		gWindow = SDL_CreateWindow("SDL Tutorial 2", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN);
		if (gWindow == NULL)
		{
			//Window failed to be created
			printf("Window could not be created. SDL_Error: %s\n", SDL_GetError());
			success = false;
		}
		else
		{
			//Create renderer for window
			gRenderer = SDL_CreateRenderer(gWindow, -1, SDL_RENDERER_ACCELERATED);
			if (gRenderer == NULL)
			{
				printf("Renderer could not be created! SDL Error: %s\n", SDL_GetError());
				success = false;
			}
			else
			{
				//Initialize renderer color
				SDL_SetRenderDrawColor(gRenderer, 0xFF, 0xFF, 0xFF, 0xFF);

				//Initialize PNG loading
				int imgFlags = IMG_INIT_PNG;
				if (!(IMG_Init(imgFlags) & imgFlags))
				{
					printf("SDL_image could not initialize! SDL_image Error: %s\n", IMG_GetError());
					success = false;
				}

				gBackground = new LTexture(gRenderer);
				gOtherImage = new LTexture(gRenderer);
			}
		}

		//Initialize SDL_mixer
		if (Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 2048) < 0)
		{
			printf("SDL_mixer could not initialize! SDL_mixer Error: %s\n", Mix_GetError());
			success = false;
		}

	}
	return success;
}

bool loadMedia()
{
	bool success = true;
	std::string path = "Resources/background.bmp";

	//Attempt to load image
	if (!gBackground->loadFromFile(path))
	{
		printf("Failed to load background texture image\n");
		success = false;
	}

	path = "Resources/otherImage.bmp";
	if (!gOtherImage->loadFromFile(path))
	{
		printf("Failed to load texture image\n");
		success = false;
	}
	else
	{
		gSpriteClips[0].x = 0;
		gSpriteClips[0].y = 0;
		gSpriteClips[0].w = 200;
		gSpriteClips[0].h = 300;

		gSpriteClips[1].x = 200;
		gSpriteClips[1].y = 0;
		gSpriteClips[1].w = 200;
		gSpriteClips[1].h = 300;

		gSpriteClips[2].x = 400;
		gSpriteClips[2].y = 0;
		gSpriteClips[2].w = 200;
		gSpriteClips[2].h = 300;
	}

	return success;
}

void close()
{
	// Deallocate and destroy
	gBackground->free();
	gOtherImage->free();

	SDL_DestroyRenderer(gRenderer);
	SDL_DestroyWindow(gWindow);
	gWindow = NULL;
	gRenderer = NULL;

	Mix_Quit();
	IMG_Quit();
	SDL_Quit();
}


int main(int argc, char* args[])
{
	bool quit = false;
	bool displayBackground = true;
	SDL_Event e;
	bool lSpriteParts[6] = { false, false, false, false, false, false };

	ScrnPs lRandomPts[3];
	

	if (!init())
	{
		printf("Failed to init.\n");
	}
	else
	{
		if (!loadMedia())
		{
			printf("Failed to load media.\n");
		}
		else
		{
			while (!quit)
			{
				//Handle events on queue
				while (SDL_PollEvent(&e) != 0)
				{
					//User requests quit
					if (e.type == SDL_QUIT)
					{
						quit = true;
					}

					else if (e.type == SDL_KEYDOWN)
					{
						switch (e.key.keysym.sym)
						{
						case SDLK_1:
							lSpriteParts[0] = !lSpriteParts[0];
							break;

						case SDLK_2:
							lSpriteParts[1] = !lSpriteParts[1];
							break;

						case SDLK_3:
							lSpriteParts[2] = !lSpriteParts[2];
							break;

						case SDLK_4:
							lSpriteParts[3] = !lSpriteParts[3];
							lRandomPts[0].x = (rand() % (SCREEN_WIDTH - gSpriteClips[0].w));
							lRandomPts[0].y = (rand() % (SCREEN_HEIGHT - gSpriteClips[0].h));
							break;

						case SDLK_5:
							lSpriteParts[4] = !lSpriteParts[4];
							lRandomPts[1].x = (rand() % (SCREEN_WIDTH - gSpriteClips[1].w));
							lRandomPts[1].y = (rand() % (SCREEN_HEIGHT - gSpriteClips[1].h));
							break;

						case SDLK_6:
							lSpriteParts[5] = !lSpriteParts[5];
							lRandomPts[2].x = (rand() % (SCREEN_WIDTH - gSpriteClips[2].w));
							lRandomPts[2].y = (rand() % (SCREEN_HEIGHT - gSpriteClips[2].h));
							break;

						case SDLK_0:
							displayBackground = !displayBackground;
							break;

							//Quite the program
						case SDLK_ESCAPE:
							quit = true;
							break;

						case SDLK_q:
							quit = true;
							break;

						}
					}
				}
				SDL_SetRenderDrawColor(gRenderer, 0xFF, 0xFF, 0xFF, 0xFF);
				SDL_RenderClear(gRenderer);
				
				if (displayBackground)
				{

					gBackground->render(0, 0, NULL);
				}
				
				if (lSpriteParts[0])
				{
					gOtherImage->render(300, 300, &gSpriteClips[0]);
				}

				if (lSpriteParts[1])
				{
					gOtherImage->render(500, 300, &gSpriteClips[1]);
				}

				if (lSpriteParts[2])
				{
					gOtherImage->render(700, 300, &gSpriteClips[2]);
				}

				if (lSpriteParts[3])
				{
					gOtherImage->render(
						lRandomPts[0].x,
						lRandomPts[0].y,
						&gSpriteClips[0]);
				}

				if (lSpriteParts[4])
				{
					gOtherImage->render(
						lRandomPts[1].x,
						lRandomPts[1].y,
						&gSpriteClips[1]);
				}

				if (lSpriteParts[5])
				{
					gOtherImage->render(
						lRandomPts[2].x,
						lRandomPts[2].y,
						&gSpriteClips[2]);
				}

				SDL_RenderPresent(gRenderer);
			}
		}

	}

	close();

	return 0;
}