#pragma once

#include <string>

#include <SDL.h>
#include <SDL_image.h>


class LTexture
{
public:
	LTexture(SDL_Renderer* pRenderer);
	~LTexture();

	bool loadFromFile(std::string path);
	void free();
	void render(int x, int y, SDL_Rect* clip);

	int getWidth();
	int getHeight();

private:
	SDL_Texture* mTexture;
	SDL_Renderer* mRenderer;

	int mWidth;
	int mHeight;
};

