#include "LTexture.h"

using namespace std;

LTexture::LTexture(SDL_Renderer* pRenderer)
: mTexture(NULL), mWidth(0), mHeight(0), mRenderer(pRenderer)
{
}


bool LTexture::loadFromFile(string path)
{
	free();

	SDL_Texture* newText = NULL;

	SDL_Surface* loadedSurface = IMG_Load(path.c_str());
	if (loadedSurface == NULL)
	{
		printf("Unable to load image %s! SDL_image Error: %s\n", path.c_str(), IMG_GetError());
	}
	else
	{
		SDL_SetColorKey(loadedSurface, SDL_TRUE, SDL_MapRGB(loadedSurface->format, 0xFF, 0xFF, 0xFF));
	}

	newText = SDL_CreateTextureFromSurface(mRenderer, loadedSurface);
	if (newText == NULL)
	{
		printf("Unable to load texture from %s! SDL_image Error: %s\n", path.c_str(), SDL_GetError());
	}
	else
	{
		mWidth = loadedSurface->w;
		mHeight = loadedSurface->h;
	}

	mTexture = newText;

	return mTexture != NULL;
}

void LTexture::free()
{
	if (mTexture != NULL)
	{
		SDL_DestroyTexture(mTexture);
		mTexture = NULL;
		mWidth = 0;
		mHeight = 0;
	}
}

void LTexture::render(int x, int y, SDL_Rect* clip)
{
	SDL_Rect renderQuad = { x, y, mWidth, mHeight };
	if (clip != NULL)
	{
		renderQuad.w = clip->w;
		renderQuad.h = clip->h;
	}

	SDL_RenderCopy(mRenderer, mTexture, clip, &renderQuad);
}

int LTexture::getWidth()
{
	return mWidth;
}

int LTexture::getHeight()
{
	return mHeight;
}

LTexture::~LTexture()
{
	free();
}
